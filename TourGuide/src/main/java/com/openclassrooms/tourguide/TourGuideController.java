package com.openclassrooms.tourguide;

import com.openclassrooms.tourguide.dto.NearbyAttraction;
import com.openclassrooms.tourguide.service.TourGuideService;
import com.openclassrooms.tourguide.user.User;
import com.openclassrooms.tourguide.user.UserReward;
import gpsUtil.location.VisitedLocation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tripPricer.Provider;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
public class TourGuideController {

	private final TourGuideService tourGuideService;

    @Autowired
    public TourGuideController(TourGuideService tourGuideService) {
        this.tourGuideService = tourGuideService;
    }

    @GetMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    @Operation(
            description = """
                    Return the last location of a given User. If there is not known location, return the current location of the User
                    """,
            summary = "Return the location of the User"
    )
    @GetMapping("/getLocation")
    public VisitedLocation getLocation(@RequestParam String userName) throws ExecutionException, InterruptedException {
    	return tourGuideService.getUserLocation(getUser(userName));
    }
    
    @Operation(
            description = """
                    This endpoints accept a userName as a query parameter and return the 5 closest attraction near the user location. The reward points are also returned.
                    """,
            summary = "Return the 5 closest attractions to a given user"
    )
    @GetMapping("/getNearbyAttractions")
    public List<NearbyAttraction> getNearbyAttractions(@RequestParam String userName) throws ExecutionException, InterruptedException {
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
    	return tourGuideService.getNearByAttractions(visitedLocation, getUser(userName));
    }

    @Operation(
            description = """
                    Return the rewards obtained by an user
                    """,
            summary = "Get given user Rewards"
    )
    @GetMapping("/getRewards")
    public List<UserReward> getRewards(@RequestParam String userName) {
    	return tourGuideService.getUserRewards(getUser(userName));
    }

    @Operation(
            description = """
                    Return all the trip deals for a given user. The trip deals depends on the reward points get by an user.
                    """,
            summary = "Return all the trip deals for a given user"
    )
    @GetMapping("/getTripDeals")
    public List<Provider> getTripDeals(@RequestParam String userName) {
    	return tourGuideService.getTripDeals(getUser(userName));
    }
    
    private User getUser(String userName) {
    	return tourGuideService.getUser(userName);
    }
   

}