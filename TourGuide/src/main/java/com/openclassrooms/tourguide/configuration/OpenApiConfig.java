package com.openclassrooms.tourguide.configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                contact = @Contact(
                        name = "Léonard MINOT",
                        email = "leonard.minot@gadz.org"
                ),
                description = "TourGuide API documentation. This is an application designed to track user's location and provide rewards and vouchers for attractions close to the user.",
                title = "TourGuide",
                version = "1.0"
        ),
        servers = {
                @Server(
                        description = "Local ENV",
                        url = "http://localhost:8080"
                )

        }
)
public class OpenApiConfig {
}
